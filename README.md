# @gitlab/eslint-config

is GitLab's shared ESLint config file to be used in all javascript projects across [GitLab](https://about.gitlab.com/).  It encapsulates our coding standards and is based primarily upon [`eslint-config-airbnb-base`](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb-base) and [`eslint-plugin-vue`](https://github.com/vuejs/eslint-plugin-vue) with the expectation that code formatting is handled separately by [prettier](https://prettier.io).

## Development quick start

1. Clone the project
1. Install [yarn](https://yarnpkg.com/en/)
1. Run `yarn install` to install the project dependencies

## Requirements
* Node.js 12.x, 14.x (or ^10.12.0)
* ESLint ^7.8.1

## Integrating

`$ npm install @gitlab/eslint-config`

after installing, make sure to add the following to your `.eslintrc` file

```
{
  "extends": [
    "@gitlab"
  ]
}
```

## Release management

This project automatically publishes the `master` branch using [semantic-release](https://github.com/semantic-release/semantic-release).
If a new commit/merge request is merged into `master` and it's commit(s) follows the [Angular Commit Message Conventions](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines),
a release will be automatically generated and published. Commits that do not follow the convention will be ignored and a release will not be made for them.
